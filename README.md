# Quick Summary #
## LevelObstacleSample ##
### Paul De Leon - Nexcra Software LLC.###

#### Overview ####
This code demonstrates a trap door hazard from a video game project I'm working on. It uses a finite state machine (FSM) with message passing capabilities to drive the hazards. The hazards themselves randomize their attributes to provide more interesting gameplay. The hazards can also be activated/deactivated as needed.

#### Sample code location ####
LevelObstacleSample/lib/game/*.js

#### How to run ####
This sample is a portion of an HTML 5 game written in Javascript using the commercial ImpactJS game engine and can be run from a browser. To run the demo locally, create a folder in your web server and navigate to the following URL: localhost/<folder>/index.html. Alternatively, I've uploaded the demo to my site and it can be accessed at www.nexcra.com/pdtrapdoordemo/index.html.